classdef RobotClass < handle
    
    properties        
        % Robot link and tool lengths
        link = [0.8, 0.24, 1.05, 0.225, 1.175, 0.2]; % [l1, ..., l6] link lengths
        tool = [0.248, 0.235]; % [x, z] global coordinates
        
        % Homogeneous transformation matrices
        H0_1; H0_2; H0_3; H0_4; H0_5; H0_6;
        
        % Jacobian matrix
        J;
    end
             
    
    methods
        function obj = RobotClass()     

        end
        
        function H0_1 = getH0_1(obj, q1)
            H0_1 = obj.RotZ(q1)*obj.TransZ(obj.link(1))*obj.TransX(obj.link(2))*obj.RotX(pi/2);
        end
        function H1_2 = getH1_2(obj, q2)
            H1_2 = obj.RotZ(q2+pi/2)*obj.TransZ(0)*obj.TransX(obj.link(3))*obj.RotX(0);
        end
        function H2_3 = getH2_3(obj, q3, q2)
            H2_3 = obj.RotZ(q3-q2)*obj.TransZ(0)*obj.TransX(obj.link(4))*obj.RotX(pi/2);
        end
        function H3_4 = getH3_4(obj, q4)
            H3_4 = obj.RotZ(q4)*obj.TransZ(obj.link(5))*obj.TransX(0)*obj.RotX(-pi/2);
        end
        function H4_5 = getH4_5(obj, q5)
            H4_5 = obj.RotZ(q5)*obj.TransZ(0)*obj.TransX(0)*obj.RotX(pi/2);
        end
        function H5_6 = getH5_6(obj, q6)
            H5_6 = obj.RotZ(q6+pi)*obj.TransZ(obj.link(6)+obj.tool(2))*obj.TransX(obj.tool(1))*obj.RotX(0);
        end
  
        function CalculateJacobian(obj)
            %disp('Running calculate Jacobian.')
            
            R0_0 = [    1   0   0;
                        0   1   0;
                        0   0   1
            ];
            T0_0 = [0; 0; 0];
            
            j1 = [  cross(R0_0*[0;0;1], obj.H0_6(1:3,4)-T0_0);
                        R0_0*[0;0;1]
            ];
        
            j2 = [  cross(obj.H0_1(1:3,1:3)*[0;0;1], obj.H0_6(1:3,4)-obj.H0_1(1:3,4));
                    obj.H0_1(1:3,1:3)*[0;0;1]
            ];
            
            j3 = [  cross(obj.H0_2(1:3,1:3)*[0;0;1], obj.H0_6(1:3,4)-obj.H0_2(1:3,4));
                    obj.H0_2(1:3,1:3)*[0;0;1]
            ];
            
            j4 = [  cross(obj.H0_3(1:3,1:3)*[0;0;1], obj.H0_6(1:3,4)-obj.H0_3(1:3,4));
                    obj.H0_3(1:3,1:3)*[0;0;1]
            ];
            
            j5 = [  cross(obj.H0_4(1:3,1:3)*[0;0;1], obj.H0_6(1:3,4)-obj.H0_4(1:3,4));
                    obj.H0_4(1:3,1:3)*[0;0;1]
            ];
        
            j6 = [  cross(obj.H0_5(1:3,1:3)*[0;0;1], obj.H0_6(1:3,4)-obj.H0_5(1:3,4));
                    obj.H0_5(1:3,1:3)*[0;0;1]
            ];
            
            obj.J = [j1 j2 j3 j4 j5 j6];
        end
        function Plot(obj, q)
            %disp('Plotting...')
            
            j = obj.ForwardKinematics(q);
            
            color_link = [0 0 1];
            linewidth_link = 2;
            
            color_joint = [1 0 0];
            size_joint = 10;
            
            for i = (1:6)
                plot3([j(1,i) j(1,i+1)], [j(2,i) j(2,i+1)],[j(3,i) j(3,i+1)], 'color', color_link, 'linewidth', linewidth_link);
                hold on
                plot3(j(1,i), j(2,i), j(3,i), 'o', 'color', color_joint, 'markerfacecolor', color_joint, 'markersize', size_joint);
            end
            
            xlabel('x');
            ylabel('y');
            zlabel('z');
            axis_limits = 3;
            xlim([-1 axis_limits])
            ylim([-axis_limits axis_limits])
            zlim([-0.1 axis_limits])
            grid on
        end
        function output = ForwardKinematics(obj, q)
            %disp('Running forward kinematics.');
            
            j0 = [0; 0; 0; 1];
            j1 = subs(obj.H0_1, obj.q(:), q) * j0;
            j2 = subs(obj.H0_2, obj.q(:), q) * j0;
            j3 = subs(obj.H0_3, obj.q(:), q) * j0;
            j4 = subs(obj.H0_4, obj.q(:), q) * j0;
            j5 = subs(obj.H0_5, obj.q(:), q) * j0;
            j6 = subs(obj.H0_6, obj.q(:), q) * j0;
            
            output = round([j0(1:3), j1(1:3), j2(1:3), j3(1:3), j4(1:3), j5(1:3), j6(1:3)], 4);
        end
        function output = InverseKinematics(obj, Pt, Pt_t)
            %disp('Running inverse kinematics.');
            
            % Desired origo to tool rotation matrix  
            R0_6 = [[0 0 1];
                    [0 1 0];
                    [-1 0 0]
            ];
            
            % Wrist point
            Pw_0 = Pt - (obj.l(6)+obj.tool(2))*R0_6(1:3, 3); % frame 0
            
            % Joint angle 1
            q(1) = atan2(Pw_0(2), Pw_0(1));
            
            % Transforming wrist position to frame 1
            Pw_1 = subs(obj.H0_1, obj.q(1), q(1))\[Pw_0; 1];
            r1 = sqrt(obj.l(5)^2 + obj.l(4)^2);
            r2 = sqrt(Pw_1(1)^2 + Pw_1(2)^2);
            D = (r2^2 - r1^2 - obj.l(3)^2)/(2*r1*obj.l(3));
            
            % Joint angles 2 and 3
            q(2) = atan2(Pw_1(2), Pw_1(1)) + acos((obj.l(3)^2 + r2^2 - r1^2)/(2*obj.l(3)*r2)) - pi/2; %#ok<*PROPLC>
            q(3) = pi/2 - atan2(sqrt(1-D^2), D) - atan2(obj.l(4), obj.l(5));
            
            % Last rotation matrix
            R0_3 = subs(obj.H0_3(1:3, 1:3), obj.q(1:3), [q(1) q(2) q(3)]);
            R3_6 = R0_3\R0_6;
                
            % Joint angles 4, 5 and 6
            q(5) = acos(R3_6(3,3));
            q(4) = atan2(R3_6(2,3), R3_6(1,3));
            q(6) = atan2(-R3_6(3,2), R3_6(3,1));
            
            q_t = subs(obj.J, obj.q(:), q(:))\Pt_t;
            
            output = round([q(:), q_t], 4);
        end
        
        function [g1, g2, g3, g4, g5, g6] = GravityVector(obj, q1, q2, q3, q4, q5, q6)
            
            q3 = q3-q2;
            
            g1 = 0;
            g2 = (103005*cos(q2 + pi/2))/16 + (2943*((87*cos(q5))/400 + (31*sin(q5))/125)*(cos(q3)*sin(q2 + pi/2) + cos(q2 + pi/2)*sin(q3) + (4967757600021511*sin(q4)*(cos(q3)*cos(q2 + pi/2) - sin(q3)*sin(q2 + pi/2)))/81129638414606681695789005144064))/2 + (150093*cos(q3)*cos(q2 + pi/2))/160 + (46107*cos(q3)*sin(q2 + pi/2))/16 + (46107*cos(q2 + pi/2)*sin(q3))/16 - (150093*sin(q3)*sin(q2 + pi/2))/160 - (2943*cos(q4)*(cos(q3)*cos(q2 + pi/2) - sin(q3)*sin(q2 + pi/2))*((31*cos(q5))/125 - (87*sin(q5))/400))/2;
            g3 = (2943*((87*cos(q5))/400 + (31*sin(q5))/125)*(cos(q3)*sin(q2 + pi/2) + cos(q2 + pi/2)*sin(q3) + (4967757600021511*sin(q4)*(cos(q3)*cos(q2 + pi/2) - sin(q3)*sin(q2 + pi/2)))/81129638414606681695789005144064))/2 + (150093*cos(q3)*cos(q2 + pi/2))/160 + (46107*cos(q3)*sin(q2 + pi/2))/16 + (46107*cos(q2 + pi/2)*sin(q3))/16 - (150093*sin(q3)*sin(q2 + pi/2))/160 - (2943*cos(q4)*(cos(q3)*cos(q2 + pi/2) - sin(q3)*sin(q2 + pi/2))*((31*cos(q5))/125 - (87*sin(q5))/400))/2;
            g4 = (14620110616863306873*cos(q4)*(cos(q3)*sin(q2 + pi/2) + cos(q2 + pi/2)*sin(q3))*((87*cos(q5))/400 + (31*sin(q5))/125))/162259276829213363391578010288128 + (2943*sin(q4)*(cos(q3)*sin(q2 + pi/2) + cos(q2 + pi/2)*sin(q3))*((31*cos(q5))/125 - (87*sin(q5))/400))/2;
            g5 = (2943*((31*cos(q5))/125 - (87*sin(q5))/400)*(sin(q3)*sin(q2 + pi/2) - cos(q3)*cos(q2 + pi/2) + (4967757600021511*sin(q4)*(cos(q3)*sin(q2 + pi/2) + cos(q2 + pi/2)*sin(q3)))/81129638414606681695789005144064))/2 + (2943*cos(q4)*(cos(q3)*sin(q2 + pi/2) + cos(q2 + pi/2)*sin(q3))*((87*cos(q5))/400 + (31*sin(q5))/125))/2;  
            g6 = 0;
            
%             syms q1 q2 q3 q4 q5 q6
%             
%             % Transformation matrix from joint to next cog
%             H1_C2 = obj.RotZ(q2+pi/2)*obj.TransZ(0)*obj.TransX(obj.link(3)/2)*obj.RotX(0);
%             H2_C3 = obj.RotZ(q3)*obj.TransZ(0)*obj.TransX(obj.link(4)/2)*obj.RotX(pi/2);
%             H3_C4 = obj.RotZ(q4)*obj.TransZ(obj.link(5)/2)*obj.TransX(0)*obj.RotX(-pi/2);
%             H4_C5 = obj.RotZ(q5)*obj.TransZ(0)*obj.TransX(0)*obj.RotX(pi/2);
%             H4_C6 = H4_C5*obj.RotZ(pi)*obj.TransZ((obj.link(6)+obj.tool(2))/2)*obj.TransX(obj.tool(1))*obj.RotX(0);
%             
%             H1_2 = obj.getH1_2(q2);
%             H1_3 = H1_2*obj.getH2_3(q3);
%             H1_4 = H1_3*obj.getH3_4(q4);
%             
%             m2 = 250;
%             m3 = 150;
%             m4 = 200;
%             m5 = 100;
%             m6 = 50;
%             g = 9.81;
%             
%             H1_C3 = H1_2*H2_C3;
%             H1_C4 = H1_3*H3_C4;
%             H1_C6 = H1_4*H4_C6;
%             
%             P1 = 0;
%             P2 = m2*g*H1_C2(2,4);
%             P3 = m3*g*H1_C3(2,4);
%             P4 = m4*g*H1_C4(2,4);
%             P5 = (m5+m6)*g*H1_C6(2,4);
%             
%             P = P1+P2+P3+P4+P5;
%             
%             g1 = diff(P,q1)
%             g2 = diff(P,q2)
%             g3 = diff(P,q3)
%             g4 = diff(P,q4)
%             g5 = diff(P,q5)
%             g6 = diff(P,q6)
%             
%             
%             g = [   subs(g1,[q1, q2, q3, q4, q5, q6],q), ...
%                     subs(g2,[q1, q2, q3, q4, q5, q6],q), ...
%                     subs(g3,[q1, q2, q3, q4, q5, q6],q), ...
%                     subs(g4,[q1, q2, q3, q4, q5, q6],q), ...
%                     subs(g5,[q1, q2, q3, q4, q5, q6],q), ...
%                     subs(g6,[q1, q2, q3, q4, q5, q6],q)       ];
%   
        end
        
        % Help functions
        function mat = RotZ(~, q)
            if q == 0
                c = 1;
                s = 0;
            elseif q == pi/2
                c = 0;
                s = 1;
            elseif q == pi
                c = -1;
                s = 0;
            elseif q == 3*pi/4
                c = 0;
                s = -1;
            else
                c = cos(q);
                s = sin(q);
            end

            mat = [[c, -s, 0, 0];
                   [s, c, 0, 0];
                   [0, 0, 1, 0];
                   [0, 0, 0, 1]];
        end
        function mat = TransZ(~, q)
            mat = [[1, 0, 0, 0];
                   [0, 1, 0, 0];
                   [0, 0, 1, q];
                   [0, 0, 0, 1]];
        end
        function mat = TransX(~, q)
            mat = [[1, 0, 0, q];
                   [0, 1, 0, 0];
                   [0, 0, 1, 0];
                   [0, 0, 0, 1]];
        end
        function mat = RotX(~, q)
            if q == 0
                c = 1;
                s = 0;
            elseif q == pi/2
                c = 0;
                s = 1;
            elseif q == pi
                c = -1;
                s = 0;
            elseif q == 3*pi/4
                c = 0;
                s = -1;
            else
                c = cos(q);
                s = sin(q);
            end

            mat = [[1, 0, 0, 0];
                   [0, c, -s, 0];
                   [0, s, c, 0];
                   [0, 0, 0, 1]];
        end
       
    end
end

